#!/usr/bin/env python

# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import time
import sys
import os
import Adafruit_Nokia_LCD as LCD
import Adafruit_GPIO.SPI as SPI

import Image
import ImageDraw
import ImageFont

# Setup GPIO ports for buttons
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

# GPIO pin numbers for buttons.  Buttons are connected to ground to need 
# to use internal pullups.  Button push will pull button down to 0
# these pn numbers are now on MSP23008, NEED TI IMPPIMENT I2C
BUTTON_LEFT = 0 
BUTTON_UP = 1
BUTTON_RIGHT = 3
BUTTON_DOWN = 2
BUTTON_SELECT = 4

# Setup button pins as inputs
buttons = [BUTTON_LEFT, BUTTON_UP, BUTTON_RIGHT, BUTTON_DOWN, BUTTON_SELECT]
for pin in buttons:
	GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)


# Raspberry Pi hardware SPI config:
DC = 23
RST = 24
SPI_PORT = 0
SPI_DEVICE = 0


# Hardware SPI usage:
disp = LCD.PCD8544(DC, RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=4000000))

# Initialize library.
disp.begin(contrast=40)

# Clear display.
disp.clear()
disp.display()

font = ImageFont.load_default()

# Dictionary of screens
screens =	{"welcome": "RobotMenu1.jpg",
		"mode": "RobotMenu2 - Mode.jpg",
		"rc": "RobotMenu2a - RC.jpg",
		"avoidObject": "RobotMenu2b - Avoid Object.jpg",
		"lineFollow": "RobotMenu2c - Line follow.jpg",
		"options": "RobotMenu3 - Options.jpg",
		"reboot": "RobotMenu3a - Reboot.jpg",
		"shutDown": "RobotMenu3b - Shutdown.jpg"}

# Display menu page
def menu(page):
	# Load image and convert to 1 bit color.
	image = Image.open(screens[page]).convert('1')
	draw = ImageDraw.Draw(image)
	# Display image.
	disp.image(image)
	disp.display()


def up_down():
	image = Image.new('1', (84, 48))
	draw = ImageDraw.Draw(image)
	draw.polygon([(42,1), (46,5), (38,5)], outline = 0, fill = 0)
	draw.polygon([(42,48), (46,43), (38,43)], outline = 0, fill = 0)	
	disp.image(image)
	disp.display()


def menu_system():
	# Display Welcome Screen
	menu('welcome')
	
	# Menu Level structure
	menuL1 = ['welcome', 'mode', 'options']
	menuL2 = ['null', 'rc', 'avoidObject', 'lineFollow']
	menuL3 = ['null', 'reboot', 'shutDown'] 
	
	# Set menus to page 0, i.e. 'welcome'
	l1page = 0
	l2page = 0
	l3page = 0
	
	#Main loop to detect key presses and cycle through menu
	try:
		while True:
			if GPIO.input(BUTTON_DOWN) == 0: #scroll down through level 1
				if l1page == 2:		# return to begining
					l1page = 0				
				else:
					l1page += 1
				menu(menuL1[l1page])
				#up_down()
				l2page = 0
				l3page = 0
				time.sleep(0.3)
	
			elif GPIO.input(BUTTON_UP) == 0: # scroll up through level 1
				if l1page == 0:		# return to begining
					l1page = 2				
				else:
					l1page -= 1
				menu(menuL1[l1page])
				l2page = 0
				l3page = 0
				time.sleep(0.3)
	
			elif GPIO.input(BUTTON_RIGHT) == 0: # select menu item
				if l1page == 1:
					if l2page == 3: 	# return to begining
						l2page = 1
					else:
						l2page += 1
					menu(menuL2[l2page])
					time.sleep(0.3)
				elif l1page == 2:
					if l3page == 2:		# return to begining
						l3page = 1
					else:
						l3page += 1
					menu(menuL3[l3page])
					time.sleep(0.3)
	
	
	
			elif GPIO.input(BUTTON_LEFT) == 0:
				if l1page == 1:
					if l2page == 1:
						l2page = 3
					else:
						l2page -= 1
					menu(menuL2[l2page])
					time.sleep(0.3)
	
				elif l1page == 2:
					if l3page == 1:
						l3page = 2
					else:
						l3page -= 1
					menu(menuL3[l3page])
					time.sleep(0.3)
	
			elif GPIO.input(BUTTON_SELECT) == 0:
				print "Menu level \nLevel 1 = ", l1page
				print "Level 2 = ", l2page
				print "Level 3 = ", l3page
				time.sleep(0.3)
	
				if l1page == 1 and l2page == 1:
					print "Mode = remote control"
					time.sleep(0.3)
	
				elif l1page == 1 and l2page == 2:
					print "Mode = Object Avoidance"
					time.sleep(0.3)
	
				elif l1page == 1 and l2page == 3:
					print "Mode = Line follower"
					time.sleep(0.3)
	
				elif l1page == 2 and l3page == 1:
					print "Mode = reboot"
					time.sleep(0.3)
	
				elif l1page == 2 and l3page == 2:
					print "Mode = shutdown"
					os.system( "poweroff" )
					time.sleep(0.3)
	
				else:
					print "option unkown"
					time.sleep(0.3)
	
	except KeyboardInterrupt:
        	GPIO.cleanup()
	
def main():
	menu_system()

if __name__ == '__main__':
	sys.exit(main())
