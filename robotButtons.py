#!/usr/bin/env python

# This code initialises the MCP23008 and dectect key presses
# It returns a list of buttons being pressed

import sys
import smbus

# Setup I2C MCP23008 GPIO ports for buttons
bus = smbus.SMBus(1)  # Rev 2 Pi uses 1

# MCP23008 hard wired to address 0x20
DEVICE  = 0x20

# Address for registers taken from data sheet
IODIR   = 0x00
IPOL    = 0x01
GPINTEN = 0x02
DEFVAL  = 0x03
INTCON  = 0x04
IOCON   = 0x05
GPPU    = 0x06
INTF    = 0x07
INTCAP  = 0x08
GPIO    = 0x09
OLAT    = 0x0A

# Constants used to detect button presses
BUTTON_LEFT     = 0
BUTTON_UP       = 1
BUTTON_DOWN     = 2
BUTTON_RIGHT    = 3
BUTTON_SELECT   = 4


# functions to help write to MCP23008
#Write values to I2C GPIO expander
def write_register(address, value):
	bus.write_byte_data(DEVICE, address, value)
	return


#Read valus from I2C GPIO expander
def read_register(address):
	result = bus.read_byte_data(DEVICE, address)
	return result

# Buttons are connected to ground to need to use internal pullups
# Button push will pull button down to 0, but using IPOL to invert
# signal, therefore button press will signal a 1 (high)


def initialise():
	write_register(IODIR, 0b00011111)   # Set first 5 pins (bits) as inputs
	write_register(GPPU, 0b00011111)    # Set first 5 pins (bits) to have interal pull ups enabled
	write_register(IPOL, 0b00011111)    # Invert the input polarity, so a 1 (high) = button pressed


def detect_button():
	input = read_register(GPIO)
	if input & 0b00000001 == 0b00000001:   # Left
		pin0 = 1
	else:
		pin0 = 0
	if input & 0b00000010 == 0b00000010:    # Up
		pin1 = 1
	else:
		pin1 = 0
	if input & 0b00000100 == 0b00000100:    # Down
		pin2 = 1
	else:
		pin2 = 0
	if input & 0b00001000 == 0b00001000:    # Right
		pin3 = 1
	else:
		pin3 = 0
	if input & 0b00010000 == 0b00010000:    # Select
		pin4 = 1
	else:
		pin4 = 0
	input_pins = [pin0, pin1, pin2, pin3, pin4]  # List of buttons, (1 if pressed)
	return input_pins


def main():
	detect_button()

if __name__ == '__main__':
	sys.exit(main())
