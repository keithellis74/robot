#!/usr/bin/env python

import RPi.GPIO as GPIO
import sys,tty,termios
import time
import gyro_test as gyro

''' GPIO output pins in pairs are (using BCM pin numbers)
	Left motor, pins 18 & 27
	Right motor, pins 17 & 22
	L293 EN, pin 09 
'''
# Setup GPIO output pins
GPIO.setmode(GPIO.BCM)			# Use BCM pin numbers
gpio_pins = {'leftMotorPin1' : 18, 'leftMotorPin2' : 27, 'rightMotorPin1' : 17,'rightMotorPin2' : 22}

# Setup all GPIO pins for output
for pin in gpio_pins:
	GPIO.setup(gpio_pins[pin], GPIO.OUT)

# GPIO pin controlling EN on motor driver, prevents motors running on boot
GPIO.setup(9,GPIO.OUT,initial=0) 

# Setup EN pin as PWM
EN = GPIO.PWM(9, 50)
EN.start(0)

# Global variable, 0 at all times except when driving forward then = "forward"
# Used in object detect interupt to ensure it is only active when driving
# forward
Drive = 0

''' Functions to control movement of robot.  Inc. stop, forward,
 reverse, fast left, fast right, slow left, slow right '''

# Stop Function - Sets duty cycle of all pins to zero
def stop():
	EN.ChangeDutyCycle(0)
	for pin in gpio_pins:
		GPIO.output(gpio_pins[pin],0)

# drive_motor fuinction, this takes dc (duty cycle or speed from 0 to 100)
# and motors, this is a LIST detailing which GPIO pins should be set to high
# gpio_pins Dictionary contains four pin numbers 
# left motor forward, left motor revers, right motor forward
# right motor reverse.

def drive_motor(dc,motors):
	stop()
	for pin in motors:
		GPIO.output(gpio_pins[pin], 1)
		EN.ChangeDutyCycle(dc)

	
#Main code below
def main():
	stop()
	print "drive forward"
	drive_motor(100, ['leftMotorPin1','rightMotorPin1']) # drive forward
	time.sleep(2.5)
	print "turn left"
	drive_motor(100, ['leftMotorPin2', 'rightMotorPin1']) # rotate left 90 degrees
	gyro.rotate(90)
	print "turn complete"  
	print "drive  forward"		
	drive_motor(100, ['leftMotorPin1', 'rightMotorPin1']) # drive forward
	time.sleep(2.5)
	drive_motor(100, ['leftMotorPin2', 'rightMotorPin2']) # drive backwards
	time.sleep(5)
	drive_motor(100, ['leftMotorPin1', 'rightMotorPin1']) # drive forwards
	time.sleep(2.5)
	drive_motor(100, ['leftMotorPin2', 'rightMotorPin1']) # rotate left 90 degrees
	gyro.rotate(90)
	drive_motor(100, ['leftMotorPin1', 'rightMotorPin1']) # drive forward
	time.sleep(2.5)
	print "stop"
	stop()
	GPIO.cleanup()
		
if __name__ == '__main__':
	sys.exit(main())
