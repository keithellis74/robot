#!/usr/bin/env python
''' 	Pi Robot main code
	Motor driver for two motors forward and reverse.
	Object advoidance with two IR sensors, one front left
	and another front right

By Keith Ellis

Original version required input from terminal as an input command

Rev 01 - 03/01/2014
Trying input from terminal stdin without haveing to press return
Rev 02 - 07/01/2014
Impliment PWM so that turning is slower and speeed is incremential
Impliment drive_motor() function, this receives ducty cycle and pins to 
set high to drive motors in desired direction
Rev 03 - 10/02/2014
Modify to put PWM on EN pin of L293D driver, and measure volt/currents, 
is it any more efficient - V/Amps no different, but keep PWM on EN 
Rev 04 - 10/02/2014
Add in Threading to detect object ahead and take avoiding action
Rev 05 - 25/02/2014
Add second thread to detect front left and front right using two object
detection IR sensors
Rev 06 - 13/03/2014
Start to impliment Wii Remote control (buttons only)
Rev 07 - ??
Wii remote works with cross hair buttons and also forward speed is controlled
with Wii remote tilting.  Vertical = Stop, horizontal = full speed forward
Rev 08 - 02/09/2014
Edited the GPIO pins so that it works with HumblePi prototyping plate
'''

import RPi.GPIO as GPIO
import time
import sys,tty,termios
import cwiid 		# Requied for Wii Remote

''' 	GPIO output pins in pairs are (using BCM pin numbers)
	Left motor, pins 18 & 27
	Right motor, pins 17 & 22
	L293 EN, pin 09 
	IR Sensor front left, pin 04
	IR Sensor front right, pin 25
	Also used by MCP23008 for I2C pins 2 & 3 	
'''
# Setup GPIO output pins
GPIO.setmode(GPIO.BCM)			# Use BCM pin numbers
gpio_pins = {'leftMotorPin1' : 18, 'leftMotorPin2' : 27, 'rightMotorPin1' : 17,'rightMotorPin2' : 22}

# Setup all GPIO pins for output
for pin in gpio_pins:
	GPIO.setup(gpio_pins[pin], GPIO.OUT)

# GPIO pin controlling EN on motor driver, prevents motors running on boot
GPIO.setup(9,GPIO.OUT,initial=0) 

# Setup EN pin as PWM
EN = GPIO.PWM(9, 50)
EN.start(0)

# Setup GPIO as input to detect object ahead using IR sensor
ir_sensor_fl = 04
ir_sensor_fr = 25
GPIO.setup(ir_sensor_fl, GPIO.IN)
GPIO.setup(ir_sensor_fr, GPIO.IN)

# Global variable, 0 at all times except when driving forward then = "forward"
# Used in object detect interupt to ensure it is only active when driving
# forward
Drive = 0

''' Functions to control movement of robot.  Inc. stop, forward,
 reverse, fast left, fast right, slow left, slow right '''


# The getch method can determine which key has been pressed
# by the user on the keyboard by accessing the system files
# It will then return the pressed key as a variable
def getch():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch


# Stop Function - Sets duty cycle of all pins to zero
def stop():
	EN.ChangeDutyCycle(0)
	for pin in gpio_pins:
		GPIO.output(gpio_pins[pin],0)

# drive_motor fuinction, this takes dc (duty cycle or speed from 0 to 100)
# and motors, this is a LIST detailing which GPIO pins should be set to high
# gpio_pins Dictionary contains four pin numbers 
# left motor forward, left motor revers, right motor forward
# right motor reverse.

def drive_motor(dc,motors):
#	stop()
	for pin in motors:
		GPIO.output(gpio_pins[pin], 1)
#	for n in range(0,dc+1):	
		EN.ChangeDutyCycle(dc)
#		time.sleep(0.01)

# Function to take avoiding action if object detected ahead
# running in seperate thread
def object_detect(channel):
	global Drive
	print Drive
	if Drive == "forward":
		avoid_object_forward(channel)

def avoid_object_forward(sensor):
	global Drive
	print sensor
	if sensor == ir_sensor_fl:
		drive_motor(80, ['rightMotorPin2']) 
		while GPIO.input(ir_sensor_fl) ==GPIO.LOW or GPIO.input(ir_sensor_fr) == GPIO.LOW:
			pass
		time.sleep(0.2)
		drive_motor(100, ['leftMotorPin1','rightMotorPin1'])
		#stop()
		#Drive = 0
	else:
		drive_motor(80, ['leftMotorPin2'])
		while GPIO.input(ir_sensor_fl) ==GPIO.LOW or GPIO.input(ir_sensor_fr) == GPIO.LOW:
			pass
		time.sleep(0.2)
		drive_motor(100, ['leftMotorPin1','rightMotorPin1'])
		#stop()
		#Drive = 0


#Function used to control the robot
def control_keys():
	global Drive
	print "Program Running, use the following keys to control"
	print "1 = Quit \nq = forward\na = reverse\nz = stop\n"
	print "\nu = slow left\n[ = slow right"
	print "o = fast left\np = fast right"
	while True:
		try:
			n = getch()
			n = n.lower()

			if n == "q":		# Forwards
				Drive = "forward"
				drive_motor(100,['leftMotorPin1','rightMotorPin1'])

			elif n == "a":		# Reverse
				Drive = 0
				drive_motor(100,['leftMotorPin2','rightMotorPin2'])

			elif n == "o":		# fast Left
				Drive = 0
				drive_motor(100,['leftMotorPin2','rightMotorPin1'])

			elif n == "p":		# fast Right
				Drive = 0
				drive_motor(100,['leftMotorPin1','rightMotorPin2'])

			elif n == "z":		# stop
				Drive = 0
				stop()

			elif n == "i":		# slow Left
				Drive = 0
				drive_motor(100,['rightMotorPin1'])

			elif n == "[":		# slow Right
				Drive = 0
				drive_motor(100,['leftMotorPin1'])

			elif n == "1": 
				print ("Program Ended")
				GPIO.cleanup()
				break
	
			n=""

		except (KeyboardInterrupt):
			GPIO.cleanup()
			break

def control_wii():
	while wiimote == True:
		buttons = wii.state["buttons"]
		acc_x = wii.state['acc'][1]
		print buttons
		# If Plus and Minus buttons pressed
		# together then rumble and quit.
		if (buttons - cwiid.BTN_PLUS - cwiid.BTN_MINUS == 0):
			print "\nClosing connection ..."
			wii.rumble = 1
			time.sleep(1)
			wii.rumble = 0
			wiimote = False
			#exit(wii)
			GPIO.cleanup()

		elif(buttons == 2048):  #  Forwards
			print "forward"
			Drive = "forward"
			if acc_x < 100:
				dc = 0
			elif acc_x > 125:
				dc = 100
			else:
				dc = ((float(acc_x)-100)/25)*100
			print 'dc = ',dc
			print 'acc_x' ,acc_x
			drive_motor(dc,['leftMotorPin1','rightMotorPin1'])
		elif(buttons & cwiid.BTN_DOWN):  #  Reverse
			print "reverse"
			Drive = 0
			drive_motor(100,['leftMotorPin2','rightMotorPin2'])
		elif(buttons == 256):  #  Slow Left
			print "slow left"
 			Drive = "forward"
			drive_motor(100,['rightMotorPin1'])
                elif(buttons == 512):  #  Slow right
			print "slow right"
			Drive = 0
			drive_motor(100,['leftMotorPin1'])
		elif(buttons == 260):  #  Fast left
			print "Fast left"
			Drive = "forward"
			drive_motor(100,['leftMotorPin2','rightMotorPin1'])
		elif(buttons == 516):  #  Fast right
			print "Fast right"
			Drive = 0
			drive_motor(100,['leftMotorPin1','rightMotorPin2'])

		else:
			stop()


def search_remote():
	# Search for Wii Remote
	print "Press 1 + 2 on your Wii Remote now..."
	time.sleep(1)
		
	#Connect to the Wii Remote.  If it times out then quit.
	try:
		wii=cwiid.Wiimote()
		wiimote=True
		wii.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC
	except RuntimeError:
		print "Error opening wiimote connection"
		print "Use keys to control"
		wiimote = False
	return wiimote


	
#Main code below
def main():	
	stop()
	#Call thread to detect object ahead
	GPIO.add_event_detect(ir_sensor_fl, GPIO.FALLING, callback=object_detect)
	GPIO.add_event_detect(ir_sensor_fr, GPIO.FALLING, callback=object_detect)
	#Search for Wii remote
	if search_remote() == True:
		control_wii()

	else:
		control_keys()
	return

if __name__ == '__main__':
	sys.exit(main())
