#!/usr/bin/python

import smbus
import math
import datetime
from time import sleep

# Power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
gyro_config  = 0x1b

def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val
        
def gyro_z():
	sleep(0.5)
	return read_word_2c(0x47) / 131

bus = smbus.SMBus(1) # or bus = smbus.SMBus(0) for Revision 1 boards
address = 0x68       # This is the address value read via the i2cdetect command

# Now wake the 6050 up as it starts in sleep mode and set internal clock to use gyro z
bus.write_byte_data(address, power_mgmt_1, 0b00000011)
bus.write_byte_data(address, power_mgmt_2, 0b00111110) # Disable everything other than gyro_z
bus.write_byte_data(address, gyro_config, 0b00000000)  # change sensitivity


def rotate(turn):
	angle = 0
	rotation = []
	start = datetime.datetime.now()
	while angle < turn and angle > -turn:
		gyro = gyro_z()
		#if gyro == 0:
		#	pass
		#else:
		rotation.append(gyro)
		delta_time = datetime.datetime.now() - start
		angle = (sum(rotation)/len(rotation))*float(delta_time.total_seconds())
		print "gyro = ", gyro, "Angle turned = ", angle, "in seconds" ,delta_time.total_seconds()
	return "done"
	


