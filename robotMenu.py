#!/usr/bin/env python

# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import time
import sys
import os
import Adafruit_Nokia_LCD as LCD
import Adafruit_GPIO.SPI as SPI

import Image
import ImageDraw
import ImageFont


# Raspberry Pi hardware SPI config for Nokia screen:
DC = 23
RST = 24
SPI_PORT = 0
SPI_DEVICE = 0


# Hardware SPI usage:
disp = LCD.PCD8544(DC, RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=4000000))

# Initialize library.
disp.begin(contrast=40)

# Clear display.
disp.clear()
disp.display()


font = ImageFont.load_default()

# Dictionary of screens
screens =	{"welcome"		: "RobotMenu1.jpg",
			"mode"			: "RobotMenu2 - Mode.jpg",
			"rc"			: "RobotMenu2a - RC.jpg",
			"avoidObject"	: "RobotMenu2b - Avoid Object.jpg",
			"lineFollow"	: "RobotMenu2c - Line follow.jpg",
			"options"		: "RobotMenu3 - Options.jpg",
			"exit"			: "RobotMenu3c - Exit.jpg",
			"reboot"		: "RobotMenu3a - Reboot.jpg",
			"shutDown"		: "RobotMenu3b - Shutdown.jpg"}

# Menu Level structure
menuL1 = ['welcome', 'mode', 'options']
menuL2 = ['null', 'rc', 'avoidObject', 'lineFollow']
menuL3 = ['null', 'exit', 'reboot', 'shutDown'] 

#Button constants
BUTTON_LEFT   = 0
BUTTON_UP     = 1
BUTTON_DOWN   = 2
BUTTON_RIGHT  = 3
BUTTON_SELECT = 4

	
def initialise():
	# Display Welcome Screen
	menu('welcome')
	
	
def clearScreen():
	# Clear display.
	disp.clear()
	disp.display()
	
	

# Display menu page
def menu(page):
	# Load image and convert to 1 bit color.
	image = Image.open(os.path.join("/home/kellis/repo/robot/", screens[page])).convert('1')
	draw = ImageDraw.Draw(image)
	# Display image.
	disp.image(image)
	disp.display()


def up_down():
	image = Image.new('1', (84, 48))
	draw = ImageDraw.Draw(image)
	draw.polygon([(42,1), (46,5), (38,5)], outline = 0, fill = 0)
	draw.polygon([(42,48), (46,43), (38,43)], outline = 0, fill = 0)	
	disp.image(image)
	disp.display()


def menu_system(button, page_level):
	#Main loop to detect key presses and cycle through menu
	if button[BUTTON_DOWN] == 1: #scroll down through level 1
		if page_level[0] == 2:		# return to begining
			page_level[0] = 0				
		else:
			page_level[0] += 1
		menu(menuL1[page_level[0]])
		page_level[1] = 0
		page_level[2] = 0
		time.sleep(0.3)

	elif button[BUTTON_UP] == 1: # scroll up through level 1
		if page_level[0] == 0:		# return to begining
			page_level[0] = 2				
		else:
			page_level[0] -= 1
		menu(menuL1[page_level[0]])
		page_level[1] = 0
		page_level[2] = 0
		time.sleep(0.3)

	elif button[BUTTON_RIGHT] == 1: # select menu item
		if page_level[0] == 1:
			if page_level[1] == 3: 	# return to begining
				page_level[1] = 1
			else:
				page_level[1] += 1
			menu(menuL2[page_level[1]])
			time.sleep(0.3)
		elif page_level[0] == 2:
			if page_level[2] == 3:		# return to begining
				page_level[2] = 1
			else:
				page_level[2] += 1
			menu(menuL3[page_level[2]])
			time.sleep(0.3)



	elif button[BUTTON_LEFT] == 1:
		if page_level[0] == 1:
			if page_level[1] <= 1:
				page_level[1] = 3
			else:
				page_level[1] -= 1
			menu(menuL2[page_level[1]])
			time.sleep(0.3)

		elif page_level[0] == 2:
			if page_level[2] <= 1:
				page_level[2] = 2
			else:
				page_level[2] -= 1
			menu(menuL3[page_level[2]])
			time.sleep(0.3)
	return page_level

def displayText(string): 
	# Parse long text into multiple lines
	# 14 charachters per line, 6 pixels per charachter
	# The function receives five strings, and prints each string to a 
	# line on the Nokia screen.  It does not check the number of
	# charachters in each string, if there are more than 14 charachers 
	#in each line they will be ignored.

	image2 = Image.new('1', (LCD.LCDWIDTH, LCD.LCDHEIGHT))
	draw = ImageDraw.Draw(image2)
	draw.rectangle((0,0,LCD.LCDWIDTH,LCD.LCDHEIGHT), outline=255, fill=255)
	draw.text((0,0), string[0] , font=font)
	draw.text((0,9), string[1] , font=font)
	draw.text((0,18), string[2] , font=font)
	draw.text((0,27), string[3] , font=font)
	draw.text((0,36), string[4] , font=font)
	disp.image(image2)
	disp.display()

def split_line(text):
	# This function splits a string of text into 5 lines of text, all 
	# lines 14 charachters or less, so they fit on the Nokia screen
	# If the string has more words than fit on the screen, they are 
	# ignored
	

	# split the text into words
	words = text.split()
	
	# declare variables to store the strings for each line.  Each stirng
	# will be 14 characters or less, to fit on a line of the screen.
	string1 = ""
	string2 = ""
	string3 = ""
	string4 = ""
	string5 = ""
	
	# temporary variable, if "complete" the line is full and the next 
	# line should be used.
	str1 = ""
	str2 = ""
	str3 = ""
	str4 = ""
	str5 = ""
	
	# cycle through each word and add to a line, if the line is complete
	# mark as "complete" and move onto the next line.
	for word in words:
		if len(string1) < 15 and str1 != "complete":
			if len(string1) + len(word) <15:
				string1 += word
				string1 +=" "
			else:
				string2 = word +" "
				str1 = "complete"
		
		elif len(string2) < 15 and str2 !="complete":
			if len(string2) + len(word) < 15: 
				string2 += word
				string2 +=" "
			else:
				string3 = word + " "
				str2 = "complete"
	
		elif len(string3) < 15 and str3 !="complete":
			if len(string3) + len(word) < 15:
				string3 += word
				string3 +=" "
			else:
				string4 = word + " "
				str3 = "complete"
	
		elif len(string4) < 15 and str4 !="complete":
			if len(string4) + len(word) <15:
				string4 += word
				string4 +=" "
			else:
				string5 = word + " "
				str4 = "complete"
				
		elif len(string5) < 15 and str5 !="complete":
			if len(string5) + len(word) <15:
				string5 += word
				string5 +=" "
			else:
				str4 = "complete"				
				
			
	#print string1
	#print string2
	#print string3
	#print string4
	#print string5
	
	# return the five lines as stirngs.  The calling function will 
	# receive a list of five stirngs
	
	return string1, string2, string3, string4, string5

	
def main():
	menu_system()

if __name__ == '__main__':
	sys.exit(main())
