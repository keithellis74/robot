#!/usr/bin/env python
# This code is intended to pull functions in from
# robotMenu.py and pi_robot.py

import pi_robot as robot
import robotMenu as menu
import robotButtons as button
import time
import os

# Set menus to page 0, i.e. 'welcome'
page_level = [0, 0, 0]

button.initialise()		# Iniitialise the I2C GPIO expander 
menu.initialise()			# Display the main menu page on LCD

InMenu = False;
robot.initialise()

print "Menu system started"
InMenu = True
while InMenu:
	# Detect button presses and save in button list
	buttons =  button.detect_button()
	
	# Send buttons to menu system and return current position within menu
	page_level = menu.menu_system(buttons, page_level)
	if buttons[4] == True:
		if page_level[0] == 1 and page_level[1] == 1:
			print "Mode = remote control"
			time.sleep(0.3)
			robot.main("rc")
			menu.menu("rc")

		elif page_level[0] == 1 and page_level[1] == 2:
			print "Mode = Object Avoidance"
			time.sleep(0.3)
			robot.main("avoidObject")
			menu.menu("avoidObject")

		elif page_level[0] == 1 and page_level[1] == 3:
			print "Mode = Line follower"
			time.sleep(0.3)

		elif page_level[0] == 2 and page_level[2] == 1:
			print "Mode = exit"
			#GPIO.cleanup()
			menu.displayText(menu.split_line("Program Exiting ....."))
			time.sleep(3)
			menu.clearScreen()
			robot.cleanup()
			break
			
		elif page_level[0] == 2 and page_level[2] == 2:
			print "Mode = reboot"
			menu.displayText(menu.split_line("Pi going down for reboot ....."))
			time.sleep(3)
			menu.clearScreen()
			os.system("reboot")
			time.sleep(0.3)

			
		elif page_level[0] == 2 and page_level[2] == 3:
			print "Mode = shutdown"
			menu.displayText(menu.split_line("Pi shutting down ....."))
			time.sleep(3)
			menu.clearScreen()
			os.system( "poweroff" )
			time.sleep(0.3)

			
		else:
			print "option unkown"
			time.sleep(0.3)

